const router = require("express").Router();

const todo = require("../controllers/todo");

router.route("/").get(todo.getTodos).post(todo.createTodo);
router
  .route("/:id")
  .put(todo.editTodo)
  .delete(todo.deleteTodo)
  .patch(todo.toggleCompletion);

module.exports = router;
