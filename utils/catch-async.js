// Helper function to prevent incessant 'try and catch'
module.exports = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};
