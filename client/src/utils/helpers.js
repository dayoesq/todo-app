import React from "react";
export const getError = (error) => {
  return error && error.length > 0 && <p style={{ color: "red" }}>{error}</p>;
};
