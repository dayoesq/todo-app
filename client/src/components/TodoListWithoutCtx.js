// This is the commonest approach to react state management

import React, { useState, useEffect, Fragment, useCallback } from "react";

import { useHttpClient } from "../hooks/http";
import { getError } from "../utils/helpers";
import Spinner from "./shared-ui/Spinner";
import Button from "../components/form-elements/button/Button";
import "./Todos.scss";

const TodoWithoutCtx = () => {
  const [todos, setTodos] = useState([]);
  const [editedTodo, setEditedTodo] = useState(null);
  const [enteredText, setEnteredText] = useState("");
  const { error, isLoading, sendRequest } = useHttpClient();

  // Fetch todos
  const getTodos = useCallback(async () => {
    const response = await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos`,
      "GET",
      null
    );
    setTodos(response.data);
  }, [sendRequest]);

  useEffect(() => {
    getTodos();
    if (editedTodo) {
      setEnteredText(editedTodo.text);
    }
  }, [getTodos, editedTodo]);

  const startEditHandler = (todo) => {
    setEditedTodo(todo);
  };

  // Add and edit todo
  const submitHandler = async (e) => {
    e.preventDefault();
    setEditedTodo(null);
    setEnteredText("");
    let url = `${process.env.REACT_APP_BACK_URL}/todos`;
    let method = "POST";
    if (editedTodo) {
      url = url + "/" + editedTodo._id;
      method = "PUT";
    }
    await sendRequest(url, method, JSON.stringify({ text: enteredText }), {
      "Content-Type": "application/json",
    });
    getTodos();
  };

  // Delete todo
  const deleteTodoHandler = async (id) => {
    await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos/${id}`,
      "DELETE",
      null
    );
    const remaininingTodos = todos.filter((todo) => todo._id !== id);
    setTodos(remaininingTodos);
  };

  // Toggle between 'done' and 'undone'
  const toggleCompleteHandler = async (todo) => {
    setEditedTodo(null);
    setEnteredText("");
    await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos/${todo._id}`,
      "PATCH",
      JSON.stringify({ isDone: !todo.isDone }),
      { "Content-Type": "application/json" }
    );
    getTodos();
  };

  return (
    <Fragment>
      {isLoading && <Spinner asOverlay />}
      {getError(error)}
      <div className="todos__form">
        <form onSubmit={submitHandler}>
          <label>
            {todos && todos.length > 0 ? "ADD TODO LIST" : "YOU HAVE NO TODOS"}
          </label>
          <input
            type="text"
            value={enteredText}
            onChange={(e) => setEnteredText(e.target.value)}
          />
          <Button
            type="submit"
            disabled={!enteredText}
            color="primary"
            shape="rounded"
          >
            {editedTodo ? "Edit" : "Add"}
            Todo
          </Button>
        </form>
      </div>
      {todos && todos.length > 0 && (
        <ul className="todos__list">
          {todos.map((todo) => (
            <li key={todo._id}>
              {todo.isDone ? (
                <span style={{ color: "green" }}>{todo.text}</span>
              ) : (
                <span>{todo.text}</span>
              )}
              <div className="todo__actions">
                {!todo.isDone && (
                  <Button
                    onClick={() => startEditHandler(todo)}
                    color="primary"
                    shape="rounded"
                  >
                    Edit
                  </Button>
                )}
                <Button
                  onClick={() => deleteTodoHandler(todo._id)}
                  color="primary"
                  shape="rounded"
                >
                  Delete
                </Button>
                <Button
                  onClick={() => toggleCompleteHandler(todo)}
                  style={{ backgroundColor: todo.isDone ? "green" : "" }}
                  color="primary"
                  shape="rounded"
                >
                  {todo.isDone ? "Done" : "Done?"}
                </Button>
              </div>
            </li>
          ))}
        </ul>
      )}
    </Fragment>
  );
};

export default TodoWithoutCtx;
