// This is the second approach with context
import React, { useContext } from 'react';

import { getError } from '../utils/helpers';
import Spinner from './shared-ui/Spinner';
import TodoContext from '../hooks/context';
import Button from '../components/form-elements/button/Button';

import './Todos.scss';

const TodoList = () => {
  const ctx = useContext(TodoContext);
  return (
    <React.Fragment>
      {ctx.isLoading && <Spinner asOverlay />}
      {getError(ctx.error)}
      <div className='todos__form'>
        <form onSubmit={ctx.submitHandler}>
          <label>{ctx.todos && ctx.todos.length > 0 ? 'ADD TODO LIST' : 'YOU HAVE NO TODOS'}</label>
          <input type='text' value={ctx.enteredText} onChange={e => ctx.setEnteredText(e.target.value)} />
          <Button
            color='primary'
            type='submit'
            shape='rounded'
            disabled={!ctx.enteredText}
          >
            {ctx.editedTodo ? 'Edit' : 'Add'}
          Todo
          </Button>
        </form>
      </div>
      {ctx.todos && ctx.todos.length > 0 && (
        <ul className='todos__list'>
          {ctx.todos.map(todo => (
            <li key={todo._id}>
              {todo.isDone ? <span style={{ color: 'green' }}>{todo.text}</span> : <span>{todo.text}</span>}
              <div className='todo__actions'>
                {!todo.isDone && <Button
                  onClick={() => ctx.startEditHandler(todo)}
                  color='primary'
                  shape='rounded'
                >
                  Edit
                </Button>}
                <Button
                  onClick={() => ctx.deleteTodoHandler(todo._id)}
                  color='primary'
                  shape='rounded'
                >
                  Delete
                </Button>
                <Button
                  color="primary"
                  shape='rounded'
                  onClick={() => ctx.toggleCompleteHandler(todo)}
                  style={{ backgroundColor: todo.isDone ? 'green' : '' }}
                >
                  {todo.isDone ? 'Done' : 'Done?'}
                </Button>
              </div>
            </li>
          ))}
        </ul>
      )}
    </React.Fragment>
  );
};

export default TodoList;

