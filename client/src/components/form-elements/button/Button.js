// Depending on the requirements, this component could be customized fully.
import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import "./Button.scss";

const Button = (props) => {
  if (props.href) {
    return (
      <a
        className={`button button--${props.size || "default"} ${
          props.color && `button--${props.color}`
        } 
        ${props.shape && `button--${props.shape}`}`}
        href={props.href}
        style={props.style}
      >
        {props.children}
      </a>
    );
  }
  if (props.to) {
    return (
      <Link
        to={props.to}
        exact={props.exact}
        className={`button button--${props.size || "default"} ${
          props.color && `button--${props.color}`
        }
        ${props.shape && `button--${props.shape}`}`}
        style={props.style}
      >
        {props.children}
      </Link>
    );
  }
  return (
    <button
      className={`button button--${props.size || "default"} ${
        props.color && `button--${props.color}`
      }
      ${props.shape && `button--${props.shape}`}`}
      type={props.type}
      onClick={props.onClick}
      disabled={props.disabled}
      style={props.style}
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  href: PropTypes.string,
  size: PropTypes.string,
  children: PropTypes.node,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  to: PropTypes.string,
  style: PropTypes.object,
  exact: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
  shape: PropTypes.string,
};

export default Button;
