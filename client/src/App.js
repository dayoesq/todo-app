import React from "react";

import TodoList from "./components/TodoList";
// import TodoWithoutCtx from "./components/TodoListWithoutCtx";

function App() {
  return <TodoList />;
  // return <TodoWithoutCtx />
}

export default App;
