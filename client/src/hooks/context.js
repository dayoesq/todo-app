import React, { useEffect, useState, useCallback } from "react";
import PropTypes from "prop-types";

import { useHttpClient } from "../hooks/http";

const TodoContext = React.createContext({
  todos: [],
  enteredText: "",
  editedTodo: null,
  deleteTodoHandler: () => {},
  submitHandler: () => {},
  startEditHandler: () => {},
  toggleCompleteHandler: () => {},
  error: null,
  setEnteredText: () => {},
  isLoading: false,
});

export const TodoListProvider = ({ children }) => {
  const [todos, setTodos] = useState([]);
  const [editedTodo, setEditedTodo] = useState(null);
  const [enteredText, setEnteredText] = useState("");
  const { error, isLoading, sendRequest } = useHttpClient();

  // Fetch todos
  const getTodos = useCallback(async () => {
    const response = await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos`,
      "GET",
      null
    );
    setTodos(response.data);
  }, [sendRequest]);

  useEffect(() => {
    getTodos();
    if (editedTodo) {
      setEnteredText(editedTodo.text);
    }
  }, [getTodos, editedTodo]);

  const startEditHandler = (todo) => {
    setEditedTodo(todo);
  };

  // Add and edit todo
  const submitHandler = async (e) => {
    e.preventDefault();
    setEditedTodo(null);
    setEnteredText("");
    let url = `${process.env.REACT_APP_BACK_URL}/todos`;
    let method = "POST";
    if (editedTodo) {
      url = url + "/" + editedTodo._id;
      method = "PUT";
    }
    await sendRequest(url, method, JSON.stringify({ text: enteredText }), {
      "Content-Type": "application/json",
    });
    getTodos();
  };

  // Delete todo
  const deleteTodoHandler = async (id) => {
    await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos/${id}`,
      "DELETE",
      null
    );
    const remaininingTodos = todos.filter((todo) => todo._id !== id);
    setTodos(remaininingTodos);
    // getTodos();
  };

  // Toggle between 'done' and 'undone'
  const toggleCompleteHandler = async (todo) => {
    setEditedTodo(null);
    setEnteredText("");
    await sendRequest(
      `${process.env.REACT_APP_BACK_URL}/todos/${todo._id}`,
      "PATCH",
      JSON.stringify({ isDone: !todo.isDone }),
      { "Content-Type": "application/json" }
    );
    getTodos();
  };

  return (
    <TodoContext.Provider
      value={{
        todos,
        enteredText,
        editedTodo,
        startEditHandler,
        submitHandler,
        toggleCompleteHandler,
        deleteTodoHandler,
        setEnteredText,
        isLoading,
        error,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};

TodoListProvider.propTypes = {
  children: PropTypes.node,
};

export default TodoContext;
