import { useState, useCallback, useRef, useEffect } from "react";

export const useHttpClient = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState([]);

  const activeHttpRequests = useRef([]);

  const sendRequest = useCallback(
    async (url, method = "GET", body = null, headers = {}) => {
      const httpAbortCtrl = new window.AbortController();
      setIsLoading(true);
      activeHttpRequests.current.push(httpAbortCtrl);
      try {
        const response = await fetch(url, {
          method,
          body,
          headers,
          signal: httpAbortCtrl.signal,
        });
        const responseData = await response.json();

        activeHttpRequests.current = activeHttpRequests.current.filter(
          (reqCtrl) => reqCtrl !== httpAbortCtrl
        );

        if (!response.ok) {
          throw new Error(responseData.message);
        }
        setIsLoading(false);
        return responseData;
      } catch (err) {
        // Push app errors into a container
        const initialError = [];
        const errors = err.message.split(".");
        if (errors.length >= 1) {
          errors.forEach((err) => {
            if (err.includes("Cast", "ObjectId")) {
              err = `Invalid ${err.split("path")[1]} id`;
            }
            initialError.push(err);
            setError(initialError);
          });
        }
        setIsLoading(false);
      }
    },
    []
  );

  const clearError = () => {
    setTimeout(() => {
      setError(null);
    }, 5000);
  };

  useEffect(() => {
    clearError();
  }, [error]);

  useEffect(() => {
    return () => {
      activeHttpRequests.current.forEach((abortCtrl) => abortCtrl.abort());
    };
  }, []);

  return {
    isLoading,
    error,
    sendRequest,
    clearError,
  };
};
