const mongoose = require("mongoose");
const app = require("./app");

// Uncaught exception error
process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION!!! Shutting down...");
  console.log(err.name, err.message);
  process.exit(1);
});

const DB = process.env.DATABASE.replace("<password>", process.env.PASSWORD);
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("DB connected!");
  });

const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => {
  console.log(`App running on port ${PORT}...`);
});

// Shut down the server in case of any unhandledRejection error
process.on("unhandledRejection", (err) => {
  console.log(err.name, err.message);
  console.log("UNHANDLED REJECTION!!!, Shutting down...");
  server.close(() => {
    process.exit(1);
  });
});
