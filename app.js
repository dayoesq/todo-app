const express = require("express");
const path = require("path");
const helmet = require("helmet");
const sanitize = require("express-mongo-sanitize");
const xssClean = require("xss-clean");
const globalError = require("./controllers/error");
const todoRouter = require("./routes/todo");
const hpp = require("hpp");

const app = express();

app.use((_req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, PATCH, DELETE");
  next();
});

app.disable("x-powered-by");
app.use(sanitize());
app.use(xssClean());
app.use(hpp());

app.use(express.static(path.join(__dirname, "client", "build")));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(
  helmet({
    referrerPolicy: { policy: "no-referrer" },
  })
);

app.use("/todos", todoRouter);

app.use((req, res, next) => {
  res.sendFile(path.resolve(__dirname, "client/build", "index.html"));
});

// Catch all errors globally
app.use(globalError);

module.exports = app;
