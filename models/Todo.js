const mongoose = require("mongoose");

const todoSchema = new mongoose.Schema({
  text: {
    type: String,
    minlength: [2, "The content is too short"],
    maxlength: [100, "The content is too long"],
    trim: true,
  },
  isDone: {
    type: Boolean,
    enum: [true, false],
    default: false,
  },
});

const Todo = mongoose.model("Todo", todoSchema);
module.exports = Todo;
