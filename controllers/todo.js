const Todo = require("../models/Todo");
const catchAsync = require("../utils/catch-async");
const AppError = require("../utils/app-error");

// Create
exports.createTodo = catchAsync(async (req, res, next) => {
  const todo = new Todo({ text: req.body.text });
  if (!todo) return next(new AppError("Something went wrong", 500));
  await todo.save();
  res.status(201).json({
    status: "success",
  });
});

// Read many
exports.getTodos = catchAsync(async (req, res, next) => {
  const todos = await Todo.find({}).exec();
  if (!todos) return next(new AppError("Something went wrong", 500));
  res.status(200).json({
    status: "success",
    data: todos,
  });
});

// Update one
exports.editTodo = catchAsync(async (req, res, next) => {
  const notFound = new AppError("Could not find document", 404);
  const updatedTodo = await Todo.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  })
    .orFail(notFound)
    .exec();
  if (!updatedTodo) return next(new AppError("Something went wrong", 500));
  res.status(200).json({
    status: "success",
  });
});

// Update one
exports.toggleCompletion = catchAsync(async (req, res, next) => {
  const notFound = new AppError("Could not find document", 404);
  const updatedTodo = await Todo.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  })
    .orFail(notFound)
    .exec();
  if (!updatedTodo) return next(new AppError("Something went wrong", 500));
  res.status(200).json({
    status: "success",
  });
});

// Delete one
exports.deleteTodo = catchAsync(async (req, res, next) => {
  const notFound = new AppError("Could not find document", 404);
  const deletedTodo = await Todo.findByIdAndDelete(req.params.id)
    .orFail(notFound)
    .exec();
  if (!deletedTodo) return next(new AppError("Something went wrong", 500));
  res.status(200).json({
    status: "success",
  });
});
