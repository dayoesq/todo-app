// Global error handler
const mongoose = require("mongoose");

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "fail";
  if (process.env.NODE_ENV === "development") {
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
      error: err,
      stack: err.stack,
    });
  } else if (process.env.NODE_ENV === "production") {
    if (err instanceof mongoose.Error.ValidationError) {
      const errors = Object.values(err.errors).map((el) => el.message);
      return res.status(400).json({
        status: err.status,
        message: errors.join(". "),
      });
    } else if (err instanceof mongoose.Error.CastError) {
      const message = `Invalid ${err.path}`;
      return res.status(err.statusCode).json({
        status: err.status,
        message,
      });
    } else if (err.code === 11000) {
      const message = "Document already exists";
      return res.status(409).json({
        status: err.status,
        message,
      });
    }
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  }
  next();
};
